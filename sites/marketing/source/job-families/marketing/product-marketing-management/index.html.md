---
layout: job_family_page
title: "Product Marketing Management"
---

### Manager, Product Marketing

The Manager, Product Marketing reports to the Director of Product Marketing.

#### Manager, Product Marketing Job Grade 

The Manager, Product Marketing is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Product Marketing Responsibilities

* Recruit, coach and manage product marketing managers that live our values
* Set organization direction, staff for scale and develop the skills and career paths of all team members
* Ensure all our product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Technical Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

#### Manager, Product Marketing Requirements

* 8-10 years of product marketing experience.
* Experience with DevOps and/or developer tooling
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

### Manager, Technical Marketing

The Manager, Technical Marketing Director of Product Marketing.  

#### Manager, Technical Marketing Job Grade 

The Manager, Technical Marketing is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Technical Marketing Responsibilities

* Recruit, coach and manage technical product marketing managers that live our [values](/handbook/values/)
* Set organization direction, staff for scale and develop the skills and career paths of all team members
* Ensure all our technical product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

#### Manager, Technical Marketing Requirements

* 5+ years of technical product marketing experience.
* Experience with DevOps and/or developer tooling
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

###  Manager, Partner and Channel Marketing

This Manager, Partner and Channel Marketing reports to the Director of Product Marketing.

#### Manager, Partner and Channel Marketing Job Grade 

The Manager, Partner and Channel Marketing is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Partner and Channel Marketing Responsibilities

* Recruit, coach and manage product marketing managers that live our values.
* Set organization direction, staff for scale and develop the skills and career paths of all team members.
* Ensure all our product marketing managers are very effective.
* Promote  and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
* Partner and work closely with Technical Product Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

#### Manager, Partner and Channel Marketing Requirements

* 8-10 years of product marketing experience.
* 1-2 years of partner, channel, reseller or alliance marketing.
* Experience with DevOps and/or developer tooling.
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

### Manager, Competitive Intelligence

The Manager, Competitive Intelligence reports to the Director of Product Marketing.

#### Manager, Competitive Intelligence Responsibilities

* Recruit, coach and manage a competitive intelligence team that lives our values.
* Set organization direction, staff for scale and develop the skills and career paths of all team members.
* Ensure all our competitive intelligence managers are effective and deliver results.
* Own the quality, effectiveness and performance of the competitive intelligence team results.
* Measure and improve the happiness and productivity of the team.
* Promote  and identify underperformance proactively.
* Partner and work closely with the Product Marketing, Technical Marketing, Product Management, Sales, Engineering, Customer Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

#### Manager, Competitive Intelligence Requirements

* 8-10 years of product marketing, technical marketing or competitive intelligence experience.
* Experience with DevOps and/or developer tooling.
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

### Manager, Market Research and Customer Insights

The Manager, Market Research and Customer Insights reports to the Director of Product Marketing.

#### Manager, Market Research and Customer Insights Job Grade 

The Manager, Partner and Channel Marketing is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Market Research and Customer Insights Responsibilities

* Recruit, coach and manage product marketing managers that live our values.
* Set organization direction, staff for scale and develop the skills and career paths of all team members.
* Ensure all our product marketing managers are very effective.
* Promote and identify underperformance proactively.
* Own the quality, effectiveness and performance of the product marketing team results.
* Measure and improve the happiness and productivity of the team.
Partner and work closely with Strategic Marketing, Product Management, Sales, Engineering, Customer -Success, and other Marketing functions.
* Make sure the success metrics such as quarterly OKRs are identified, met, and communicated across key stakeholders.

#### Manager, Market Research and Customer Insights Requirements

* 8-10 years of product marketing experience.
* Past experience in market and customer research roles.
* Knowledge around, and an ability to help prepare for industry analyst briefings as required.
* Experience with DevOps and/or developer tooling.
* Team player with strong intra-personal skills, skilled at project management and cross-functional collaboration.
* 5+ years experience in hiring, scaling, managing and leading a team.
* Understand the difference between managing and leading a team.
* Experience managing external vendors and agencies.
* Enterprise software company experience.
* Passionate about open source and developer tools.
* Strong communication skills.
* You share our values, and work in accordance with those values.
* Ability to use GitLab.

### Director, Global Partner Marketing

The Director, Global Partner Marketing reports to 

#### Director, Global Partner Marketing Job Grade 

The Director, Global Partner Marketing is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Global Partner Marketing Responsibilities

* Develop worldwide partner marketing strategy, develop GTM partner marketing plans, metrics and KPIs in alignment with FY22 planning and GTM goals for Partner Initiated Opportunities (PIOs) that are both incremental to our business or represent growth opportunities with existing customers.
* Work with Technology and Ecosystem alliance partners to identify, develop and execute joint Go To Market (GTM) campaigns.  Identify and integrate Channel partners into these campaigns as appropriate.
* Lead a team of partner marketers focused on developing scalable, global campaigns aligned to our approved major GTM motions, developed and executed jointly with partners and our demand generation, field marketing, corporate marketing teams to drive demand resulting in PIOs.
* Develop unique marketing campaigns and messaging to attract and recruit high value partners to become Gitlab channel partners. 
* Develop Technology, Ecosystem and Channel Partner  marketing engagement strategy to build share of voice and sales preference for GitLab with partner sales, sales engineers, marketing and C-level.  
* Implement channel partner marketing enablement initiatives, inclusive of programs, tactics, communication vehicles, and enablement tools that will help Channel Partners drive increased market adoption and channel revenue goals.
* Manage our Market Development Fund (MDF) program including strategy, rules, requests and reimbursement processes.
* Assist our Channel Sales Managers in the development of actionable and measurable marketing programs for designated Select and Open channel partners.
* Work with our Demand Generation Campaigns team for content and campaigns that we can enable partners with that support a Top of Funnel (TOFU) land and post sale expand strategy to broaden the GitLab footprint within each of their accounts.  
* Strategize and work with field marketing on regional partner joint GTM market execution to help progress current marketing qualified leads to an opportunity.
* Working with our Marketing and Channel Sales Ops teams develop ROI performance dashboards for partner marketing initiatives and utilize findings to improve future initiatives.
* Develop and execute monthly partner marketing webcast enablement sessions  to provide a steady flow of content and demand generation tactics that partners can leverage.
* Develop and manage the rollout of partner marketing systems used to facilitate partners ability to market, and generate trials of GitLab software.
Work with FP&A to ensure MDF allocations/marketing spend are appropriately allocated for.
* Measurement:
   * Develop joint Technology, Ecosystem and Channel Partner campaigns and programs that support partner demand generation efforts and that are aligned to GitLab’s fiscal year approved GTM motions.  Monitor and measure activity effectiveness through the generation of Inquiries, MQLs, SAOs, Closed Wons developed jointly with Technology and Ecosystem partners, and Channel Qualified Leads and Partner Initiated Opportunities. 
   * Provide a strong leadership voice and input on Technology, Ecosystem and Channel Partner GTM effectiveness and relative performance in management review meetings.
   * Ensure above satisfactory ratings of GitLab Partner Marketing effectiveness in partner surveys.
   * Meet or exceed quarterly PIO goals (new and growth).  Monitor, measure and track lead lifecycle, close rates, lead effectiveness from co and joint marketing campaign efforts with Technology, Ecosystem and Channel Partners.

#### Director, Global Partner Marketing Requirements

* Minimum of 10+ years of experience in partner marketing with increasing levels of responsibility.
* Specific experience with Technology, Ecosystem and Channel partners that participate in the DevOps market.
* Proven track record in the partner community building successful programs and campaigns to support technology alliances sales, channel sales and channel partners.  
* Understanding of Software Development and DevOps; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* Outstanding communications skills to work across a remote, matrixed environment. 
* Able to coordinate across many teams and iterate and deliver in a fast-moving startup environment.
* Proven experience being self-directed and working with minimal supervision.
* Data-driven, use data to measure results and inform decision making and strategy development.
* Experience with Software-as-a-Service offerings and open core software a plus.
* You share our values and work according to those values.
* Ability to use GitLab.
* Ability to use Slack.
* Ability to use Google Docs.

## Career Ladder

The next step for managers of people is to move to the [Director of Product Marketing](/job-families/marketing/director-product-marketing) job family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates may be invited to interview with the Hiring Manager
* Next, candidates may be invited to interview with 2-4 Team Members
* Finally, candidates may be invited to interview with someone from leadership

Additional details about our process can be found on our [hiring page](/handbook/hiring).
